export interface MenuData {
  key: string
  text: string
  children?: MenuData[]
}
